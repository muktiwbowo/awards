package com.muktiwibowo.awards

import android.os.Parcel
import android.os.Parcelable

class ModelHome(var image: String?, var point: String?, var type: String?, var gift: String?): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(image)
        parcel.writeString(point)
        parcel.writeString(type)
        parcel.writeString(gift)
    }

    override fun describeContents(): Int = 0

    companion object CREATOR : Parcelable.Creator<ModelHome> {
        override fun createFromParcel(parcel: Parcel): ModelHome {
            return ModelHome(parcel)
        }

        override fun newArray(size: Int): Array<ModelHome?> {
            return arrayOfNulls(size)
        }
    }
}