package com.muktiwibowo.awards

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class AdapterHome(): RecyclerView.Adapter<HolderHome>() {
    private var items: ArrayList<ModelHome> = arrayListOf()

    fun getHomeItems(): ArrayList<ModelHome>{
        return items
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderHome {
        return HolderHome(LayoutInflater.from(parent.context).inflate(R.layout.holder_home, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: HolderHome, position: Int) {
        holder.bindHomeItems(items[position])
    }
}