package com.muktiwibowo.awards

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.home_content.*


class ActivityHome : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private lateinit var drawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        initView()
        initItems()
    }

    fun initView(){
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toolbar.setTitleTextColor(resources.getColor(android.R.color.black, null))
        }
        toolbar.title = ""
        setSupportActionBar(toolbar)
        drawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        toggle.isDrawerIndicatorEnabled = false
        val drawable = ResourcesCompat.getDrawable(resources, R.drawable.ic_menu_black_24dp, this.getTheme())
        toggle.setHomeAsUpIndicator(drawable)
        toggle.setToolbarNavigationClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START)
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)
    }

    fun initItems(){
        val linearLayoutManager = LinearLayoutManager(this)
        home_recycler_view.layoutManager = linearLayoutManager
        val adapterHome = AdapterHome()
        adapterHome.getHomeItems().addAll(dummyItems())
        home_recycler_view.adapter = adapterHome
    }

    fun dummyItems(): ArrayList<ModelHome>{
        val items = arrayListOf<ModelHome>()
        items.add(ModelHome("", "500.000 Poin", "Vouchers", "Gift Card IDR 1.000.000"))
        items.add(ModelHome("", "500.000 Poin", "Vouchers", "Gift Card IDR 1.000.000"))
        items.add(ModelHome("", "500.000 Poin", "Products", "Gift Card IDR 1.000.000"))
        items.add(ModelHome("", "500.000 Poin", "Products", "Gift Card IDR 1.000.000"))
        items.add(ModelHome("", "500.000 Poin", "Vouchers", "Gift Card IDR 1.000.000"))
        items.add(ModelHome("", "500.000 Poin", "Products", "Gift Card IDR 1.000.000"))
        items.add(ModelHome("", "500.000 Poin", "Vouchers", "Gift Card IDR 1.000.000"))
        items.add(ModelHome("", "500.000 Poin", "Products", "Gift Card IDR 1.000.000"))
        return items
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_home_filter, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.filter -> startActivity(Intent(this, ActivityFilter::class.java))
        }
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> {
                finish()
            }
            R.id.nav_logout -> {
                startActivity(Intent(this, ActivitySignIn::class.java))
                finish()
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }
}
