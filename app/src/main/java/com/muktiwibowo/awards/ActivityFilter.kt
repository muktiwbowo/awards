package com.muktiwibowo.awards

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.CheckBox
import android.widget.RadioGroup
import android.widget.SeekBar
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.activity_filter.*

class ActivityFilter : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)
        initToolbar()
        initViews()
    }

    fun initToolbar(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            toolbar.setTitleTextColor(resources.getColor(android.R.color.black, null))
        }
        toolbar.title = "Filter"
        setSupportActionBar(toolbar)
    }

    fun initViews(){
        seek_bar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                progress_start.text = "IDR $progress - 500000"
                point_selected.text = "Point: $progress"
                point_selected.visibility = View.VISIBLE
                clear_all.visibility = View.VISIBLE
            }

        })

        point_selected.setOnClickListener {
            point_selected.visibility = View.GONE
        }

        type_selected.setOnClickListener {
            type_selected.visibility = View.GONE
        }

        clear_all.setOnClickListener {
            point_selected.visibility = View.GONE
            type_selected.visibility = View.GONE
            clear_all.visibility = View.GONE
            vouchers_check.isChecked = false
            products_check.isChecked = false
        }

        vouchers_check.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                val vouchers = v as CheckBox
                if(vouchers.isChecked){
                    type_selected.visibility = View.VISIBLE
                    clear_all.visibility = View.VISIBLE
                    type_selected.text = "Type: Vouchers"
                }
            }

        })

        products_check.setOnClickListener(object: View.OnClickListener{
            override fun onClick(v: View?) {
                val products = v as CheckBox
                if(products.isChecked){
                    type_selected.visibility = View.VISIBLE
                    clear_all.visibility = View.VISIBLE
                    type_selected.text = "Type: Products"
                }
            }

        })

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_home_close, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.close -> startActivity(Intent(this, ActivityHome::class.java))
        }
        return true
    }
}
