package com.muktiwibowo.awards

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class ActivitySignIn : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        home_sign_in.setOnClickListener {
            checkEmail(SpannableStringBuilder(home_email.text.toString()).toString())
        }
    }

    fun checkEmail(email: String){
        Log.e("Email", ""+email)
        if (email.equals("member.id")){
            startActivity(Intent(this, ActivityHome::class.java))
        } else {
            Toast.makeText(this, "Email Address is not exists"+email, Toast.LENGTH_SHORT).show()
        }
    }
}
