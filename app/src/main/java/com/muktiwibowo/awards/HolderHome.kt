package com.muktiwibowo.awards

import android.content.res.ColorStateList
import android.os.Build
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.holder_home.view.*

class HolderHome(view: View): RecyclerView.ViewHolder(view) {
    fun bindHomeItems(modelHome: ModelHome){
        Glide.with(itemView.context)
            .load(modelHome.image)
            .into(itemView.holder_home_image)
        itemView.holder_home_point.text = modelHome.point
        itemView.holder_home_gift.text  = modelHome.gift
        itemView.holder_home_type.text  = modelHome.type
        if (modelHome.type.equals("Vouchers")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                itemView.holder_home_type.backgroundTintList = ColorStateList.valueOf(itemView.resources.getColor(android.R.color.holo_blue_dark,null))
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                itemView.holder_home_type.backgroundTintList = ColorStateList.valueOf(itemView.resources.getColor(android.R.color.holo_orange_dark,null))
            }
        }
    }
}